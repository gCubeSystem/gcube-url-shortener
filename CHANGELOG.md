
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.5.1] - 2020-11-17

#### Fixes

[21267] Fixed the slf4j-log4j12 dependency  
Moved to gcube-bom 2.0.1

## [v1.5.0] - 2019-11-26

[Task #16415] Migrate to Firebase Dynamic Links APIs


## [v1.2.0] - 2017-06-14

[Support #8940] src/it/simple-it/pom.xm


## [v1.1.1] - 2016-12-19

removed lo4j dependency from pom


## [v1.1.0] - 2016-09-27

removed jump of scope during read of Runtime HTTP-URL-Shortener

removed constructor with the parameter scope


## [v1.0.2] - 2016-06-30

removed maven portal bom dependency


## [v1.0.1] - 2016-04-26

maven-bom replaced by portal-bom


## [v1.0.0] - 2014-10-13

First Release
